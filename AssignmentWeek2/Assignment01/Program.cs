﻿using System;

namespace Assignment01
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			String FirstName;
			Console.WriteLine ("Enter You First Name");
			FirstName = Console.ReadLine ();

			String LastName;
			Console.WriteLine ("Enter You Last Name");
			LastName = Console.ReadLine ();

			String Age;
			Console.WriteLine ("Enter You Age ");
			Age = Console.ReadLine ();

			String Faculty;
			Console.WriteLine ("Enter You Faculty");
			Faculty = Console.ReadLine ();

			Console.WriteLine("My name is {0} {1}. I am {2} years old. \n I am studying in faculty of {3} at Sripatum University",FirstName,LastName,Age,Faculty);
			Console.ReadLine ();
		}
	}
}
