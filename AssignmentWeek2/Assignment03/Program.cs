﻿using System;

namespace Assignment03
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Console.WriteLine ("\t\tProgram Celsius To Fahrenheit ");
			String CelsiusAsText;
			float Celsius = 0.5f;
			Console.Write ("\n \t \t  Enter Temp of Celsius   ");
			CelsiusAsText = Console.ReadLine ();
			Celsius = float.Parse (CelsiusAsText);
			Console.WriteLine ("\n \n \t \t{0} Celsius = {1} Fahrenheit ",Celsius,Celsius*2.12);
			Console.ReadLine();
		}
	}
}
