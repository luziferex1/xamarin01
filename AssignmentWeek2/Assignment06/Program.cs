﻿using System;

namespace Assignment06
using System;

namespace No6
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			string n1Text, n2Text, n3Text;
			int n1, n2, n3;

			Console.Write ("Input Num1 :");
			n1Text =Console.ReadLine ();
			n1 = int.Parse (n1Text);

			Console.Write ("Input Num2 :");
			n2Text =Console.ReadLine ();
			n2 = int.Parse (n2Text);

			Console.Write ("Input Num3 :");
			n3Text =Console.ReadLine ();
			n3 = int.Parse (n3Text);

			int sum;
			sum = n1 + n2 + n3;
			Console.WriteLine ("Summation :{0}+{1}+{2} ={3} and Average :{3}/3 = {4}", n1, n2, n3, sum, sum / 3);
			Console.ReadLine ();
		}
	}
}

