﻿using System;

namespace Assignment04
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			string basenumtext;
			string divtext;
			int basenum;
			int div;

			Console.Write ("\t\tInput You Number  ");
			basenumtext = Console.ReadLine ();

			Console.Write ("\t\tInput Num To Div with  ");
			divtext = Console.ReadLine ();

			basenum = int.Parse (basenumtext);
			div = int.Parse (divtext);

			Console.WriteLine ("\t\t{0} div {1} = {2} & Fraction = {3}", basenum, div, basenum / div, basenum % div);
			Console.ReadLine ();
		}
	}
}
