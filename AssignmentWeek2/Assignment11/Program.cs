﻿using System;

namespace Assignment11
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			string DText, HText, MText, SText;
			int D, H, M, S;
			Console.Write ("Input Day : ");
			DText =Console.ReadLine ();
			Console.Write ("Input Hours : ");
			HText =Console.ReadLine ();
			Console.Write ("Input Min : ");
			MText =Console.ReadLine ();
			Console.Write ("Input Sec : ");	
			SText =Console.ReadLine ();
			D = int.Parse (DText);
			H = int.Parse (HText);
			M = int.Parse (MText);
			S = int.Parse (SText);

			int totalsec;
			totalsec = (D * 24)+H;   
			totalsec = (totalsec*60)+M;
			totalsec = (totalsec*60)+S;
			Console.WriteLine ("{0} Day {1} Hours {2} Minutes {3} Seconds =  {4} Seconds",D,H,M,S,totalsec);
			Console.ReadLine ();
		}
	}
}

