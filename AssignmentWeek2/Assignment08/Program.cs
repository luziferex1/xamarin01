﻿using System;

namespace Assignment08
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			int number = 0;
			string temp = "";

			Console.WriteLine ("Enter number: ");
			temp = Console.ReadLine ();
			number = int.Parse (temp);

			int n1 = number % 10;
			number = number / 10;

			int n2 = number % 10;
			number = number / 10;

			int n3 = number % 10;
			number = number / 10;

			int n4 = number % 10;

			Console.WriteLine ("Digit #1: {0}", n1);
			Console.WriteLine ("Digit #2: {0}", n2);
			Console.WriteLine ("Digit #3: {0}", n3);
			Console.WriteLine ("Digit #4: {0}", n4);
			Console.ReadLine ();
		}
	}
}
