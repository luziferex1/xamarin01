﻿using System;

namespace Assignment10
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			string NumAsText;
			int num;
			Console.Write ("Enter Second :");
			NumAsText = Console.ReadLine ();
			num = int.Parse (NumAsText);
			int sec;
			int min;
			int hour;
			sec = num % 60;
			min = (num / 60) % 60;
			hour = (num / 60 )/ 60;
			Console.WriteLine ("{0} second = {1} hour {2} min {3}sec", num, hour, min, sec);
			Console.ReadLine ();
		}
	}
}
