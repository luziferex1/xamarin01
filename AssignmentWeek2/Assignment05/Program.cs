﻿using System;

namespace Assignment05
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			string WidthText;
			string HighText;
			int Width;
			int High;

			Console.Write ("Enter width of square  ");
			WidthText =Console.ReadLine ();
			Console.Write ("Enter High of square  ");
			HighText = Console.ReadLine ();

			Width = int.Parse (WidthText);
			High = int.Parse (HighText);

			Console.WriteLine ("Area :{0}*{1} = {2}  And Perimeter :{0}+{1} = {3}", Width, High, Width * High, Width + High);
			Console.ReadLine ();
		}
	}
}
